package logica.SO;

import java.awt.Color;

import javax.swing.JOptionPane;

import logica.Cola;
import logica.Proceso;

public class SO {
	
	//Tiempo en el cual se encuentra el programa
	int tiempoActual = 0 ;
	
	//Colas empleadas por el programa
	Cola listos = new Cola();
	Cola finalizados = new Cola();
	Cola bloqueados = new Cola();
	
	/*Componentes del so*/
	Depuracion depuracion = new Depuracion(this);
	Datos datos = new Datos(this);
	GestionProcesos gestionProcesos = new GestionProcesos(this);
	GestionBloqueados  gestionBloqueados = new GestionBloqueados(this);
	
	/**
	 * Se obtiene el tiempo maximo que tendra la simulacion
	 * @return tiempo de simulacion
	 */
	public int getTiempoMaximo() {
		return 30;
	}
	
	
	
	/**********************************************************/
	/**********************************************************/
	/*******************BLOQUEO DE PROCESOS********************/
	/**********************************************************/
	/**********************************************************/
	
	
	


	
	/**********************************************************/
	/**********************************************************/
	/**************************GETTERS*************************/
	/**********************************************************/
	/**
	 * Obtener el tiempo actual del SO
	 * @return tiempo actual de la simulacion
	 */
	public int getTiempoActual() {
		return tiempoActual;
	}
	/**
	 * Obtencion de la cola de listos formateada en un String
	 * @return String que contiene la cola
	 */
	public String getColaListosFormateada() {
		String salida ="";
		salida+="Cola listos: ";
		for(int i = 0  ; i < listos.getTamano() ; i++) {
			
			salida+=listos.getElemento(i).getNombre();
			salida += "(";
			salida+= listos.getElemento(i).getT_rafaga();
			salida += ")";
			salida +=" , ";
		}
		return salida;
	}
	/**
	 * Obtencion de la cola de finalizados formateada en un String
	 * @return String que contiene la cola
	 */
	public String getColaFinalizadosFormateada() {
		String salida ="";
		salida+="Cola finalizados: ";
		for(int i = 0  ; i < finalizados.getTamano() ; i++) {
			salida+=finalizados.getElemento(i).getNombre();
			salida += "(";
			salida+= finalizados.getElemento(i).getT_rafaga();
			salida += ")";
			salida +=" , ";
		

		}
		return salida;
	}
	
	/**
	 * Obtencion de la cola de bloqueados formateada en un String
	 * @return String que contiene la cola
	 */
	public String getColaBloqueadosFormateada() {
		String salida ="";
		salida+="Cola bloqueados: ";
		for(int i = 0  ; i < bloqueados.getTamano() ; i++) {
			salida+=bloqueados.getElemento(i).getNombre();
			salida += "(";
			salida+= bloqueados.getElemento(i).getT_rafaga();
			salida += ")";
			salida +=" , ";
		

		}
		return salida;
	}
	public Cola getColaListos() {
		return listos;
	}
	public Cola getColaFinalizados() {
		return finalizados;
	}
	public Cola getColaBloqueados() {
		return bloqueados;
	}
	
	public Datos datos() {
		return datos;
	}
	
	public Depuracion depuracion() {
		return depuracion;
	}
	public GestionProcesos gestionProcesos() {
		return gestionProcesos;
	}
	public GestionBloqueados gestionBloqueados() {
		return gestionBloqueados;
	}
	
	public void aumentarTiempoActual(int tiempoActual) {
		this.tiempoActual += tiempoActual;
	}
	
	
}
