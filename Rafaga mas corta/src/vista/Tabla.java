package vista;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class Tabla {
	JPanel panel;
	
	JTable tabla;
	JScrollPane scroll;
	String[] columnas = {"PROCESO","TIEMPO LLEGADA","RAFAGA","TIEMPO COMIENZO","TIEMPO FINAL","TIEMPO RETORNO","TIEMPO ESPERA"};
	DefaultTableModel modelo;
	
	public Tabla() {

		panel = new JPanel();
		panel.setLayout(new BorderLayout());
		String[][] datos = {{"A","0","1","1","1","1","1"},{"B","1","1","1","1","1","1"},{"1","1","1","1","1","1","1"}};
		tabla= new JTable(new DefaultTableModel(datos, columnas));
		scroll=new JScrollPane(tabla);
		panel.add(scroll,BorderLayout.CENTER);
	}
	public void repintarTabla(String[][] datos) {
		tabla.setModel(new DefaultTableModel(datos, columnas));

	}
	public JPanel getPanel() {
		return panel;
	}
}
