package vista;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import logica.SO.SO;

public class Colas {
	JLabel listos;
	JLabel finalizados;
	JLabel bloqueados;
	
	JPanel panel;
	SO so;
	public Colas(SO so) {
		this.so=so;
		listos = new JLabel("cola listos");
		finalizados = new JLabel("cola finalizados");
		bloqueados = new JLabel ("cola bloqueados");
		
		panel= new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add(listos);
		panel.add(finalizados);
		panel.add(bloqueados);
	}
	
	public JPanel getPanel() {
		return panel;
	}
	public void actualizarColas() {
		listos.setText(so.getColaListosFormateada());
		finalizados.setText(so.getColaFinalizadosFormateada());
		bloqueados.setText(so.getColaBloqueadosFormateada());
	}
}
